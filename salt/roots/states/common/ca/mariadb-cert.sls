{% set aws_bucket = salt['pillar.get']('aws:bucket:name')+'/'+salt['pillar.get']('aws:bucket:location') -%}

include:
  - common.ca.ca

ensure-mariadb-cert-dir:
  file.directory:
    - name: /root/pki/issued_certs/mariadb
    - require:
      - file: ensure-ca-subdir

create-mariadb-private-key:
  x509.private_key_managed:
    - name: /root/pki/issued_certs/mariadb/mariadb.key
    - bits: 2048
    - backup: True
    - require:
      - file: ensure-mariadb-cert-dir

create-mariadb-csr:
  cmd.run:
    - name: openssl req -new -key /root/pki/issued_certs/mariadb/mariadb.key -out /root/pki/issued_certs/mariadb/mariadb.csr -subj /CN=mariadb.example.com
    - unless: ls /root/pki/issued_certs/mariadb/mariadb.csr
    - require:
      - x509: create-mariadb-private-key

create-mariadb-cert:
  x509.certificate_managed:
    - name: /root/pki/issued_certs/mariadb/mariadb.pem
    - days_remaining: 0
    - backup: True
    - days_valid: 3600
    - csr: /root/pki/issued_certs/mariadb/mariadb.csr
    - signing_cert: /root/pki/ca.crt
    - signing_private_key: /root/pki/ca.key
    - require:
      - cmd: create-mariadb-csr
      - x509: create-mariadb-private-key
      - x509: create-ca-cert

upload-mariadb-cert:
  cmd.run:
    - name: aws s3 sync /root/pki/issued_certs/mariadb s3://{{ aws_bucket }}/mariadb
    - require:
      - x509: create-mariadb-cert

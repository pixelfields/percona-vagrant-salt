{% set aws_bucket = salt['pillar.get']('aws:bucket:name')+'/'+salt['pillar.get']('aws:bucket:location') -%}

include:
  - common.ca.ca

ensure-percona1-cert-dir:
  file.directory:
    - name: /root/pki/issued_certs/percona1
    - require:
      - file: ensure-ca-subdir

create-percona1-private-key:
  x509.private_key_managed:
    - name: /root/pki/issued_certs/percona1/percona1.key
    - bits: 2048
    - backup: True
    - require:
      - file: ensure-percona1-cert-dir

create-percona1-csr:
  cmd.run:
    - name: openssl req -new -key /root/pki/issued_certs/percona1/percona1.key -out /root/pki/issued_certs/percona1/percona1.csr -subj /CN=percona1.example.com
    - unless: ls /root/pki/issued_certs/percona1/percona1.csr
    - require:
      - x509: create-percona1-private-key

create-percona1-cert:
  x509.certificate_managed:
    - name: /root/pki/issued_certs/percona1/percona1.pem
    - days_remaining: 0
    - backup: True
    - days_valid: 3600
    - csr: /root/pki/issued_certs/percona1/percona1.csr
    - signing_cert: /root/pki/ca.crt
    - signing_private_key: /root/pki/ca.key
    - require:
      - cmd: create-percona1-csr
      - x509: create-percona1-private-key
      - x509: create-ca-cert

upload-percona1-cert:
  cmd.run:
    - name: aws s3 sync /root/pki/issued_certs/percona1 s3://{{ aws_bucket }}/percona1
    - require:
      - x509: create-percona1-cert

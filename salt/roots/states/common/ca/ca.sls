{% set aws_bucket = salt['pillar.get']('aws:bucket:name')+'/'+salt['pillar.get']('aws:bucket:location') -%}

include:
  - common.aws

gcc-installed:
  pkg.installed:
    - name: gcc

ssl-dev-install:
  pkg.installed:
    {% if salt['grains.get']('os_family') == 'RedHat' %}
    - name: openssl-devel
    {% else %}
    - name: libssl-dev
    {% endif %}

swig-install:
  pkg.installed:
    - name: swig

m2crypto-install:
  pip.installed:
    - name: M2Crypto < 0.25.0
    - require:
      - pkg: pip-install
      - pkg: python-dev-install
      - pkg: ssl-dev-install
      - pkg: swig

ensure-ca-dir:
  file.directory:
    - name: /root/pki

ensure-ca-subdir:
  file.directory:
    - name: /root/pki/issued_certs
    - require:
      - file: ensure-ca-dir

create-ca-key:
  x509.private_key_managed:
    - name: /root/pki/ca.key
    - bits: 4096
    - backup: True
    - require:
      - pip: m2crypto-install
      - file: ensure-ca-dir

create-ca-cert:
  x509.certificate_managed:
    - name: /root/pki/ca.crt
    - signing_private_key: /root/pki/ca.key
    - CN: ca.example.com
    - C: US
    - ST: Utah
    - L: Salt Lake City
    - basicConstraints: "critical CA:true"
    - keyUsage: "critical cRLSign, keyCertSign"
    - subjectKeyIdentifier: hash
    - authorityKeyIdentifier: keyid,issuer:always
    - days_valid: 3650
    - days_remaining: 0
    - backup: True
    - require:
      - x509: create-ca-key

upload-ca-cert:
  cmd.run:
    - name: aws s3 cp /root/pki/ca.crt s3://{{ aws_bucket }}/ca.crt
    - require:
      - x509: create-ca-cert

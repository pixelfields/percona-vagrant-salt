pip-install:
  pkg.installed:
    - name: python-pip

python-dev-install:
  pkg.installed:
    {% if salt['grains.get']('os_family') == 'RedHat' %}
    - name: python-devel
    {% else %}
    - name: python-dev
    {% endif %}

awscli-install:
  pip.installed:
    - name: awscli

ensure-aws-dir:
  file.directory:
    - name: /root/.aws

awscli-credentials:
  file.managed:
    - name: /root/.aws/credentials
    - source: salt://files/aws/credentials.template
    - template: jinja
    - require:
      - pip: awscli-install
      - file: ensure-aws-dir

awscli-config:
  file.managed:
    - name: /root/.aws/config
    - source: salt://files/aws/config.template
    - template: jinja
    - require:
      - file: awscli-credentials


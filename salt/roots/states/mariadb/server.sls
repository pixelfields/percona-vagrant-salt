{% set aws_bucket = salt['pillar.get']('aws:bucket:name')+'/'+salt['pillar.get']('aws:bucket:location') -%}

include:
  - mariadb.repository
  - common.aws

install-mariadb:
  pkg.installed:
    - name: mariadb-server-5.5

copy-ca-cert:
  file.copy:
    - name: /etc/mysql/ca.pem
    - source: /root/pki/ca.crt
    - user: mysql
    - group: mysql
    - require:
      - x509: create-ca-cert
      - pkg: install-mariadb

copy-mariadb-cert:
  file.copy:
    - name: /etc/mysql/mariadb.pem
    - source: /root/pki/issued_certs/mariadb/mariadb.pem
    - user: mysql
    - group: mysql
    - require:
      - x509: create-mariadb-cert
      - pkg: install-mariadb

copy-mariadb-key:
  file.copy:
    - name: /etc/mysql/mariadb.key
    - source: /root/pki/issued_certs/mariadb/mariadb.key
    - user: mysql
    - group: mysql
    - require:
      - x509: create-mariadb-private-key
      - pkg: install-mariadb

configure-mariadb:
  file.managed:
    - name: /etc/mysql/my.cnf
    - source: salt://files/mariadb-my.cnf
    - require:
      - pkg: install-mariadb
      - x509: create-mariadb-cert

mysql-running:
  service.running:
    - name: mysql
    - watch:
      - file: configure-mariadb
      - x509: create-mariadb-cert

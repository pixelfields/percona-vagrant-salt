{% set first_run = salt['grains.get']('first-run', '') %}
{% set backup_dir = salt['grains.get']('backup-dir', '/tmp/backups/engine-r') %}
{% set backup_subdir = salt['cmd.run']('ls ' + backup_dir ) %}
{% set percona1_ip = salt['grains.get']('percona1-ip', '') %}

ensure_backup_dir:
  file.directory:
    - name: {{ backup_dir }}
    - makedirs: True
    - user: vagrant
    - group: vagrant
    - recurse:
      - user
      - group

xtra_backup_run:
  cmd.run:
    {% if first_run == '' %}
    - name: innobackupex --user=root {{ backup_dir }}
    {% else %}
    - name: innobackupex --user=root --apply-log {{ backup_dir }}/{{ backup_subdir }}
    {% endif %}

{% if first_run == '' %}

set_first_run_grain:
  grains.present:
    - name: first-run
    - value: True
    - require:
      - cmd: xtra_backup_run

set_backup_dir_grain:
  grains.present:
    - name: backup-dir
    - value: {{ backup_dir }}
    - require:
      - cmd: xtra_backup_run

{% endif %}

vagrant_ssh_key:
  file.managed:
    - name: /home/vagrant/.ssh/id_rsa
    - source: salt://files/ssh/vagrant
    - user: vagrant
    - group: vagrant
    - mode: 600

{% if percona1_ip != '' and first_run != '' %}

copy_backup_to_node1:
  cmd.run:
    - name: rsync -avpP -e "ssh -o IdentityFile=/home/vagrant/.ssh/id_rsa" {{ backup_dir }}/{{ backup_subdir }} {{ percona1_ip }}:/tmp/backups/engine-r
    - require:
      - cmd: xtra_backup_run
      - file: vagrant_ssh_key

{% endif %}

get-mariadb-repo:
  pkgrepo.managed:
    - humanname: MariaDB Repo
    - name: deb http://mirror.hosting90.cz/mariadb/repo/5.5/debian wheezy main
    - keyserver: keyserver.ubuntu.com
    - keyid: 1BB943DB
    - refresh_db: True

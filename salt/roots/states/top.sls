base:
  'role:mariadb':
    - match: grain
    - mariadb.server
  'role:percona1':
    - match: grain
    - percona.repository
  'role:percona2':
    - match: grain
    - percona.repository
  'role:percona3':
    - match: grain
    - percona.repository

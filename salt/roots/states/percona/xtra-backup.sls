include:
  - percona.repository

install-xtra-backup:
  pkg.installed:
    - name: percona-xtrabackup

rsync-installed:
  pkg.installed:
    - name: rsync

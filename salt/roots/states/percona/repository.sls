{% set debian_release = salt['cmd.run']('lsb_release -sc') %}

percona_repository_package:
  pkg.installed:
    - sources:
      {% if salt['grains.get']('os_family') == 'RedHat' %}
      - percona-release: http://www.percona.com/downloads/percona-release/redhat/0.1-3/percona-release-0.1-3.noarch.rpm
      {% else %}
      - percona-release: https://repo.percona.com/apt/percona-release_0.1-4.{{ debian_release }}_all.deb
      {% endif %}


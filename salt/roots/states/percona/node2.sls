{% set aws_bucket = salt['pillar.get']('aws:bucket:name')+'/'+salt['pillar.get']('aws:bucket:location') -%}

ssl-dev-install:
  pkg.installed:
    - name: libssl-dev

include:
 - percona.repository
 - common.aws

debconf-utils:
  pkg.installed

percona-xtradb-cluster-server-5.5_setup:
  debconf.set:
    - name: percona-xtradb-cluster-server-5.5
    - data:
        'percona-xtradb-cluster-server-5.5/root_password': {'type': 'password', 'value': '' }
        'percona-xtradb-cluster-server-5.5/root_password_again': {'type': 'password', 'value': '' }
    - require:
      - pkg: debconf-utils

python-mysqldb:
  pkg.installed

percona_cluster_installed:
  pkg.installed:
    - name: percona-xtradb-cluster-55
    - require:
      - pkg: percona_repository_package
      - debconf: percona-xtradb-cluster-server-5.5
      - pkg: python-mysqldb

mysqld_stopped:
  service.dead:
    - name: mysql
    - require:
      - pkg: percona_cluster_installed

backup_original_mysql_conf:
  file.copy:
    - name: /etc/mysql/my.cnf.orig
    - source: /etc/mysql/my.cnf
    - require:
      - service: mysqld_stopped

percona_cluster_config_file:
  file.managed:
    - name: /etc/mysql/my.cnf
    - source: salt://files/percona2-my.cnf
    - require:
      - file: backup_original_mysql_conf

download_ca_cert:
  cmd.run:
    - name: aws s3 cp s3://{{ aws_bucket }}/percona1/ca.crt /etc/mysql/ca.crt
    - unless: ls /etc/mysql/ca.crt
    - require:
      - file: percona_cluster_config_file

download_percona1_cert:
  cmd.run:
    - name: aws s3 cp s3://{{ aws_bucket }}/percona1/percona1.pem /etc/mysql/percona1.pem
    - unless: ls /etc/mysql/percona1.pem
    - require:
      - file: percona_cluster_config_file

download_percona1_cert_key:
  cmd.run:
    - name: aws s3 cp s3://{{ aws_bucket }}/percona1/percona1.key /etc/mysql/percona1.key
    - unless: ls /etc/mysql/percona1.key
    - require:
      - file: percona_cluster_config_file

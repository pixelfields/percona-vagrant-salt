{% set aws_bucket = salt['pillar.get']('aws:bucket:name')+'/'+salt['pillar.get']('aws:bucket:location') -%}

include:
 - percona.repository
 - common.aws

MySQL-python:
  pkg.installed

percona_cluster_installed:
  pkg.installed:
    - name: Percona-XtraDB-Cluster-55
    - require:
      - pkg: percona_repository_package
      - pkg: MySQL-python

mysqld_stopped:
  service.dead:
    - name: mysql@bootstrap
    - require:
      - pkg: percona_cluster_installed

backup_original_mysql_conf:
  file.copy:
    - name: /etc/my.cnf.orig
    - source: /etc/my.cnf
    - require:
      - service: mysqld_stopped

percona_cluster_config_file:
  file.managed:
    - name: /etc/my.cnf
    - source: salt://files/percona1-my.cnf
    - template: jinja
    - require:
      - file: backup_original_mysql_conf

ensure-mysql-conf-dir:
  file.directory:
    - name: /etc/mysql
    - user: mysql
    - group: mysql
    - require:
      - pkg: percona_cluster_installed

download_ca_cert:
  cmd.run:
    - name: aws s3 cp s3://{{ aws_bucket }}/ca.crt /etc/mysql/ca.crt
    - unless: ls /etc/mysql/ca.crt
    - require:
      - file: percona_cluster_config_file

download_percona1_cert:
  cmd.run:
    - name: aws s3 cp s3://{{ aws_bucket }}/percona1/percona1.pem /etc/mysql/percona1.pem
    - unless: ls /etc/mysql/percona1.pem
    - require:
      - file: percona_cluster_config_file

download_percona1_cert_key:
  cmd.run:
    - name: aws s3 cp s3://{{ aws_bucket }}/percona1/percona1.key /etc/mysql/percona1.key
    - unless: ls /etc/mysql/percona1.key
    - require:
      - file: percona_cluster_config_file

ensure-certs-owner:
  cmd.run:
    - name: chown -R mysql:mysql /etc/mysql
    - onlyif: ls -lhR /etc/mysql/ | grep -i root

cluster_running:
  cmd.run:
    - name: systemctl start mysql@bootstrap
    - unless: systemctl status mysql@bootstrap
    - require:
      - pkg: percona_cluster_installed
      - file: percona_cluster_config_file

cluster_reload_configuration:
  cmd.wait:
    - name: systemctl restart mysql@bootstrap
    - watch:
      - file: percona_cluster_config_file

ensure-mysql-enabled:
  service.enabled:
    - name: mysql@bootstrap

